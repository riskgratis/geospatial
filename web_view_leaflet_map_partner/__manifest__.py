{
    "name": "Leaflet Map View for Partners (OpenStreetMap)",
    "summary": "TODO",
    "version": "12.0.1.0.1",
    "development_status": "Alpha",
    "author": "GRAP, Odoo Community Association (OCA)",
    "website": "https://github.com/OCA/geospatial",
    "license": "AGPL-3",
    "category": "Extra Tools",
    "depends": [
        "web_view_leaflet_map",
        "contacts",
    ],
    "data": [
        "views/res_partner.xml",
    ],
    "demo": [
        "demo/res_partner.xml",
    ],
    "installable": True,
    "maintainers": [
        "legalsylvain",
    ],
}
